package com.test.ideaware.api

import com.test.ideaware.BuildConfig
import com.test.ideaware.models.Fixture
import io.reactivex.Observable
import retrofit2.http.GET

interface FixturesService {

    @GET(BuildConfig.FIXTURE_ENDPOINT)
    fun getAllFixtures(): Observable<List<Fixture>>

    @GET(BuildConfig.RESULTS_ENDPOINT)
    fun getAllResults(): Observable<List<Fixture>>
}
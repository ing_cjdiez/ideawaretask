package com.test.ideaware.ui.fixtures

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.ideaware.R
import com.test.ideaware.models.Fixture
import androidx.databinding.DataBindingUtil
import com.test.ideaware.common.annotations.Constants
import com.test.ideaware.databinding.FixtureItemBinding

class FixturesAdapter: RecyclerView.Adapter<FixturesAdapter.ViewHolder>() {

    var fixturesList: ArrayList<Fixture> = ArrayList()
    private lateinit var binding: FixtureItemBinding
    private lateinit var option: Constants.FixturesResults

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.fixture_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return fixturesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(fixturesList[pos], option)
    }

    fun updateFixtures(fixtures: List<Fixture>, opt: Constants.FixturesResults){
        option = opt
        fixturesList.clear()
        fixturesList.addAll(fixtures)
        notifyDataSetChanged()
    }

    class ViewHolder(private val fixtureItemBinding: FixtureItemBinding/*, private val fixturesViewModel: FixturesViewModel*/):
            RecyclerView.ViewHolder(fixtureItemBinding.root){
        private val viewModel = FixtureItemViewModel()

        fun bind(fixture: Fixture, option: Constants.FixturesResults){
            viewModel.bind(fixture, itemView.context, option)
            fixtureItemBinding.viewModel = viewModel
            fixtureItemBinding.cvFixtureItem.tag = fixture
        }
    }
}
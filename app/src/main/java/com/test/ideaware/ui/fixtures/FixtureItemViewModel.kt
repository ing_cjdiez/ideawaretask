package com.test.ideaware.ui.fixtures

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.ideaware.R
import com.test.ideaware.common.annotations.Constants
import com.test.ideaware.common.annotations.Constants.POSTPONED
import com.test.ideaware.common.annotations.Constants.WINNER_AWAY
import com.test.ideaware.common.annotations.Constants.WINNER_HOME
import com.test.ideaware.common.extensions.toDateTimeItem
import com.test.ideaware.common.extensions.toDateToDayOfMonth
import com.test.ideaware.common.extensions.toDateToDayOfWeek
import com.test.ideaware.models.Fixture
import javax.inject.Inject

class FixtureItemViewModel @Inject constructor(): ViewModel() {

    val competitionName = MutableLiveData<String>()
    val venueName = MutableLiveData<String>()
    val dateTime = MutableLiveData<String>()
    val homeName = MutableLiveData<String>()
    val awayName = MutableLiveData<String>()
    val dayOfMonth = MutableLiveData<String>()
    val dayOfWeek = MutableLiveData<String>()
    val showPostponed = MutableLiveData<Int>()
    val postponedColor = MutableLiveData<Int>()
    val homeColor = MutableLiveData<Int>()
    val awayColor = MutableLiveData<Int>()


    fun bind(fixture: Fixture, context: Context, option: Constants.FixturesResults){
        competitionName.value = fixture.competitionStage.competition.name
        venueName.value = fixture.venue.name
        dateTime.value = fixture.date.toDateTimeItem()

        homeColor.value = ContextCompat.getColor(context, R.color.black)
        awayColor.value = ContextCompat.getColor(context, R.color.black)

        if(option== Constants.FixturesResults.FIXTURES){
            dayOfMonth.value = fixture.date.toDateToDayOfMonth()
            dayOfWeek.value = fixture.date.toDateToDayOfWeek().toUpperCase()
        }else{
            dayOfMonth.value = fixture.score?.home.toString()
            dayOfWeek.value = fixture.score?.away.toString()

            if(fixture.score?.winner== WINNER_HOME){
                homeColor.value = ContextCompat.getColor(context, R.color.blue_winner)
            }else{
                awayColor.value = ContextCompat.getColor(context, R.color.blue_winner)
            }
        }

        homeName.value = fixture.homeTeam.name
        awayName.value = fixture.awayTeam.name

        if(fixture.state==POSTPONED){
            showPostponed.value = View.VISIBLE
            postponedColor.value = ContextCompat.getColor(context, R.color.red)
        }else{
            showPostponed.value = View.INVISIBLE
            postponedColor.value = ContextCompat.getColor(context, R.color.gray_text)
        }
    }
}
package com.test.ideaware.ui

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.test.ideaware.R
import com.test.ideaware.common.annotations.Constants
import com.test.ideaware.common.extensions.activityViewModelProvider
import com.test.ideaware.ui.fixtures.FixturesFragment
import com.test.ideaware.ui.fixtures.FixturesViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)

        val fixturesViewModel1: FixturesViewModel = activityViewModelProvider(modelFactory)
        val fixturesViewModel2: FixturesViewModel = activityViewModelProvider(modelFactory)

        findViewById<TextView>(R.id.bt_fix).setOnClickListener{
            changeFragment(fixturesViewModel1, Constants.FixturesResults.FIXTURES)
        }

        findViewById<View>(R.id.bt_res).setOnClickListener{
            changeFragment(fixturesViewModel2, Constants.FixturesResults.RESULTS)
        }

        //default
        changeFragment(fixturesViewModel1, Constants.FixturesResults.FIXTURES)
    }

    private fun changeFragment(fixturesViewModel: FixturesViewModel, option: Constants.FixturesResults){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, FixturesFragment.newInstance(fixturesViewModel, option), "fix")
            .commit()
    }
}
package com.test.ideaware.ui.fixtures

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.ideaware.common.annotations.Constants.IO
import com.test.ideaware.common.annotations.Constants.UI
import com.test.ideaware.common.errors.NetworkErrorType
import com.test.ideaware.common.events.SingleLiveEvent
import com.test.ideaware.common.interfaces.ApiResponse
import com.test.ideaware.common.interfaces.ObservableCallback
import com.test.ideaware.common.states.ListViewState
import com.test.ideaware.models.Fixture
import com.test.ideaware.repositories.FixturesRepository
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import javax.inject.Named

public class FixturesViewModel @Inject constructor(
    private val fixtureRepository: FixturesRepository,
    @Named(UI) private val uiScheduler: Scheduler,
    @Named(IO) private val ioScheduler: Scheduler
): ViewModel(), ApiResponse<List<Fixture>?, Throwable>, ObservableCallback{

    var viewState = MutableLiveData<ListViewState>()

    fun fetchFixtures(){
        fixtureRepository.getAllFixtures()
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                { list -> onResponseReceived(list) },
                { error -> onFailure(error) },
                { onFinish() },
                { onStart() }
            )
    }

    fun fetchResults(){
        fixtureRepository.getAllResults()
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                { list -> onResponseReceived(list) },
                { error -> onFailure(error) },
                { onFinish() },
                { onStart() }
            )
    }

    override fun onResponseReceived(response: List<Fixture>?) {
        var fixtures = response
        viewState.value = if(fixtures!!.isNotEmpty()) ListViewState.HasData(fixtures) else ListViewState.NoData    }

    override fun onFailure(error: Throwable) {
        val errorType = if (error is TimeoutException) NetworkErrorType.TIMEOUT else NetworkErrorType.UNKNOWN
        viewState.value = ListViewState.Error(errorType)
    }

    override fun onStart() {
        viewState.value = ListViewState.Loading
    }

    override fun onFinish() {
        viewState.value = ListViewState.FinishLoading
    }
}
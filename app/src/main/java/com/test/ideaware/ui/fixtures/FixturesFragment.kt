package com.test.ideaware.ui.fixtures

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.ideaware.R
import com.test.ideaware.common.annotations.Constants
import com.test.ideaware.common.extensions.toDateHeader
import com.test.ideaware.common.states.ListViewState
import com.test.ideaware.models.Fixture
import kotlinx.android.synthetic.main.fixture_layout.view.tv_date
import kotlinx.android.synthetic.main.fixture_layout.view.rv_fixtures
import kotlinx.android.synthetic.main.fixture_layout.view.tv_loading

class FixturesFragment(private val viewModel: FixturesViewModel, private val option: Constants.FixturesResults): Fragment() {

    private lateinit var binding: View

    private val fixturesAdapter = FixturesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
        loadInfo()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflater.inflate(R.layout.fixture_layout, container, false)

        binding.rv_fixtures.adapter = fixturesAdapter
        binding.rv_fixtures.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        binding.rv_fixtures.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val firstCompletelyVisibleItemPosition = (binding.rv_fixtures.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                binding.tv_date.text = fixturesAdapter.fixturesList[firstCompletelyVisibleItemPosition].date.toDateHeader()
            }
        })

        return binding
    }

    private fun loadInfo(){
        when(option){
            Constants.FixturesResults.FIXTURES -> {
                viewModel.fetchFixtures()
            }
            Constants.FixturesResults.RESULTS -> {
                viewModel.fetchResults()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(fixturesViewModel: FixturesViewModel, option: Constants.FixturesResults): FixturesFragment {
            return FixturesFragment(fixturesViewModel, option)
        }
    }

    fun notifyUpdate(){
        loadInfo()
    }

    private fun handleStateChange(state: ListViewState?){
        when(state){
            is ListViewState.Loading -> { binding.tv_loading.visibility = View.VISIBLE }
            is ListViewState.HasData<*> -> {
                binding.tv_loading.visibility = View.GONE
                fixturesAdapter.updateFixtures((state as ListViewState.HasData<Fixture>).dataList, option)
                binding.tv_date.text = fixturesAdapter.fixturesList[0].date.toDateHeader()
            }
            else -> binding.tv_loading.visibility = View.GONE
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun handleItemClick(fixture: Fixture){
        /*
        val intent = Intent(activity, FixtureDetailsActivity::class.java)
        intent.putExtra(PARAM_FIXTURE, post)
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity, fixturesAdapter.getBinding().cvFixtureItem as View, "fix_item").toBundle())
        */
    }
}
package com.test.ideaware.repositories

import com.test.ideaware.api.FixturesService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FixturesRepository  @Inject constructor(private val fixturesService: FixturesService) {
    fun getAllFixtures() = fixturesService.getAllFixtures()
    fun getAllResults() = fixturesService.getAllResults()
}
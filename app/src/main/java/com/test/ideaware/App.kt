package com.test.ideaware

import android.app.Activity
import androidx.multidex.MultiDexApplication
import com.test.ideaware.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App: MultiDexApplication(), HasActivityInjector{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger(){
        DaggerAppComponent.builder().create(this).inject(this)
    }
}


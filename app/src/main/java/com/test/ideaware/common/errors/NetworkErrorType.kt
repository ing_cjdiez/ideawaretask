package com.test.ideaware.common.errors

enum class NetworkErrorType {
    TIMEOUT,
    UNKNOWN
}
package com.test.ideaware.common.interfaces

interface ObservableCallback {
    fun onStart()
    fun onFinish()
}
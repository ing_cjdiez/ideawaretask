package com.test.ideaware.common.extensions

import java.text.SimpleDateFormat
import java.util.*

const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

fun String.toDateHeader(): String{
    return formatDateFromString(DATE_FORMAT, "MMMM yyyy", this)
}

fun String.toDateTimeItem(): String{
    return formatDateFromString(DATE_FORMAT, "MMM d',' yyyy 'at' HH:mm", this)
}

fun String.toDateToDayOfMonth(): String{
    return formatDateFromString(DATE_FORMAT, "dd", this)
}

fun String.toDateToDayOfWeek(): String{
    return formatDateFromString(DATE_FORMAT, "EEE", this)
}

private fun formatDateFromString(inputFormat: String, outputFormat: String, inputDate: String): String{
    var parsed: Date? = null
    var outputDate = ""

    val dfInput = SimpleDateFormat(inputFormat, Locale.getDefault())
    val dfOutput = SimpleDateFormat(outputFormat, Locale.getDefault())

    try {
        parsed = dfInput.parse(inputDate)
        outputDate = dfOutput.format(parsed)
    } catch (e: Exception) {}

    return outputDate
}
package com.test.ideaware.common.annotations

object Constants {
    const val UI = "ui"
    const val IO = "io"

    const val PARAM_FIXTURE = "param_fix"
    const val POSTPONED = "postponed"
    const val WINNER_HOME = "home"
    const val WINNER_AWAY = "away"

    enum class FixturesResults {
        FIXTURES,
        RESULTS
    }
}
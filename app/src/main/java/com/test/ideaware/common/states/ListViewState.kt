package com.test.ideaware.common.states

import com.test.ideaware.common.errors.NetworkErrorType
import com.test.ideaware.common.interfaces.State

sealed class ListViewState : State {

    /**
     * Represents loading state
     */
    object Loading : ListViewState()
    object FinishLoading : ListViewState()

    /**
     * Represents no data
     */
    object NoData : ListViewState()

    /**
     * Represents a successful load
     */
    class HasData<T>(var dataList: List<T>) : ListViewState()
    class HasSingleData<T>(var dataList: T) : ListViewState()

    /**
     * Represents a failed load
     */
    class Error(var networkErrorType: NetworkErrorType) : ListViewState()
}
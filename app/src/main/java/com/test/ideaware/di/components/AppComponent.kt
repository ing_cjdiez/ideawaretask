package com.test.ideaware.di.components

import com.test.ideaware.App
import com.test.ideaware.di.modules.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        ApiModule::class,
        AppModule::class,
        NetworkModule::class,
        SchedulerModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent: AndroidInjector<App>{
    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<App>()
}
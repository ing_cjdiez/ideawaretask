package com.test.ideaware.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.ideaware.common.annotations.ViewModelKey
import com.test.ideaware.di.factory.ViewModelFactory
import com.test.ideaware.ui.fixtures.FixtureItemViewModel
import com.test.ideaware.ui.fixtures.FixturesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@SuppressWarnings("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FixturesViewModel::class)
    internal abstract fun bindFixturesViewModel(fixturesViewModel: FixturesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FixtureItemViewModel::class)
    internal abstract fun bindFixtureItemViewModel(fixtureItemViewModel: FixtureItemViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
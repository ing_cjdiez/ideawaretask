package com.test.ideaware.di.modules

import android.content.Context
import com.test.ideaware.App
import dagger.Module
import dagger.Provides

@Module
class AppModule{

    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }
}
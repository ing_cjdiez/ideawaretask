package com.test.ideaware.di.modules

import com.test.ideaware.api.FixturesService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
object ApiModule{

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideFixturesService(retrofit: Retrofit): FixturesService{
        return retrofit.create(FixturesService::class.java)
    }
}
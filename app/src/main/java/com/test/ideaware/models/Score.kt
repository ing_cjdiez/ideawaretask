package com.test.ideaware.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Score(
    @Json(name = "home") val home: Int,
    @Json(name = "away") val away: Int,
    @Json(name = "winner") val winner: String? = ""
): Serializable
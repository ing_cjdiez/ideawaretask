package com.test.ideaware.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class TeamInfo(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "shortName") val shortName: String,
    @Json(name = "abbr") val abbr: String,
    @Json(name = "alias") val alias: String
): Serializable
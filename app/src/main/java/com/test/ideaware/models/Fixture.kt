package com.test.ideaware.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Fixture(
    @Json(name = "id") val id: Int,
    @Json(name = "type") val type: String,
    @Json(name = "homeTeam") val homeTeam: TeamInfo,
    @Json(name = "awayTeam") val awayTeam: TeamInfo,
    @Json(name = "date") val date: String,
    @Json(name = "competitionStage") val competitionStage: CompetitionStage,
    @Json(name = "venue") val venue: Venue,
    @Json(name = "score") val score: Score?,
    @Json(name = "state") val state: String? = ""
):Serializable
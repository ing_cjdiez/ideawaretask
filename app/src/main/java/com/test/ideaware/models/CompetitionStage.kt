package com.test.ideaware.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class CompetitionStage(
    @Json(name = "competition") val competition: Competition
): Serializable
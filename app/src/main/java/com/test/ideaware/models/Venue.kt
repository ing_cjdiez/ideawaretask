package com.test.ideaware.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Venue(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String
): Serializable